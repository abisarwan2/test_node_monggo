const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const requestSchema = new Schema({
    condition: {type: String, required: true},
    project_type: {type: String, required: true},
    due_date: {type: Date, required: true},
    currency: {type: String, required: true},
    budget: {type: String, required: true},
    details: {type: String, required: true},
    // email: {type: String, required: true},
    // lang: {type: String, required: true},
});

module.exports = mongoose.model('Request', requestSchema);