const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/user');
const path = require('path');

router.get('/', (req, res) => {
    let position = path.join(__dirname, '../pages/login_reg.html');
    res.sendFile(position);
});
router.post('/signup', userCtrl.signup);
router.post('/login', userCtrl.login);

module.exports = router;