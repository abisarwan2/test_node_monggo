const express = require('express');
const router = express.Router();
const path = require('path');
const donneeCtrl = require('../controllers/donnee');



router.get('/', (req, res) => {
    const targetPostForm = `${req.protocol}://${req.get('host')}/test/donnee/sendData`;
    const page = path.join(__dirname, '../pages/requete.ejs');
    res.render(page, {targetPostForm : targetPostForm});
})



router.post('/sendData', donneeCtrl.sendData);


// router.post('/sendRequete', (req, res) => {
//     console.log(req.body);
// });
module.exports = router;