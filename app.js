const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const donneeRoutes = require('./routes/donnee');
const userRoutes = require('./routes/user');

app.set('view engine', 'ejs')


mongoose.connect('mongodb+srv://motdepass:motdepass@testing15aout.vjqpxou.mongodb.net/?retryWrites=true&w=majority',
    { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});


app.get('/', (req, res) => {
    res.sendFile(__dirname + '/pages/index.html');
})

app.use('/test/donnee', donneeRoutes);
app.use('/test/loginReg', userRoutes);

const donneeCtrl = require('./controllers/donnee');
app.post('/sendRequete', donneeCtrl.sendData);
module.exports = app;