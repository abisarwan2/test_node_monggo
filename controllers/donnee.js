const Request = require('../models/Request');

exports.sendData = (req, res, next) => {
    console.log(req.body);
    const request = new Request({
        condition: req.body.condition,
        project_type: req.body.project_type,
        due_date: req.body.due_date,
        currency: req.body.currency,
        budget: req.body.budget,
        details: req.body.details,
        email: req.body.email,
        lang: req.body.lang,
    });
    request.save()
        .then(() => res.status(201).json({ message: 'Donnée envoyée !' })
            .console.log("data enregistrée en base de données"))
        .catch(error => res.status(400).json({ error }));
}